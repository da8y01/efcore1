﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace EFCore1.Models;

public partial class Efcore1ContextAnnotations : DbContext
{
    public Efcore1ContextAnnotations()
    {
    }

    public Efcore1ContextAnnotations(DbContextOptions<Efcore1ContextAnnotations> options)
        : base(options)
    {
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=DESKTOP-2AS75HO; Initial Catalog=EFCore1; Persist Security Info=True; User ID=sa; Password=SQLServer2022.; encrypt=false");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
