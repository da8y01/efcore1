﻿using System;
using System.Collections.Generic;

namespace EFCore1.Entities;

public partial class OrdenVentum
{
    public int OrdenventaId { get; set; }

    public int ProductoId { get; set; }

    public int ClienteId { get; set; }

    public DateTime? ReturnDate { get; set; }

    public int EmpleadoId { get; set; }

    public byte[]? LastUpdate { get; set; }

    public virtual Cliente Cliente { get; set; } = null!;

    public virtual Empleado Empleado { get; set; } = null!;

    public virtual Producto Producto { get; set; } = null!;
}
