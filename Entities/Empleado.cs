﻿using System;
using System.Collections.Generic;

namespace EFCore1.Entities;

public partial class Empleado
{
    public int EmpleadoId { get; set; }

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public string? Email { get; set; }

    public string Username { get; set; } = null!;

    public string? Password { get; set; }

    public byte[]? LastUpdate { get; set; }

    public virtual ICollection<OrdenVentum> OrdenVenta { get; set; } = new List<OrdenVentum>();
}
