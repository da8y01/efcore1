﻿using System;
using System.Collections.Generic;

namespace EFCore1.Entities;

public partial class Post
{
    public int Id { get; set; }

    public string Content { get; set; } = null!;

    public DateTime CreatedTimestamp { get; set; }
}
