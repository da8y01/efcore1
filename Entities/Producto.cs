﻿using System;
using System.Collections.Generic;

namespace EFCore1.Entities;

public partial class Producto
{
    public int ProductoId { get; set; }

    public int LineaproductoId { get; set; }

    public string Name { get; set; } = null!;

    public virtual LineaProducto Lineaproducto { get; set; } = null!;

    public virtual ICollection<OrdenVentum> OrdenVenta { get; set; } = new List<OrdenVentum>();
}
